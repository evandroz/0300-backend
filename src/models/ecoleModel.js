const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const ecoleSchema = new Schema({
    "adresse": String,
    "nom": String,
    "localite": String,
    "npa": Number,
    "moniteurs": [
      {
        "civilite": String,
        "nom": String,
        "prenom": String,
        "adresse": String,
        "dateNaissance": Date,
        "mails": [
          {
            "mail": String,
            "type": String
          }
        ],
        "telephones": [
          {
            "numero": String,
            "type": String
          }
        ],
        "_id": {
          "$oid": String
        }
      }
    ]
  }, { collection: "ecole" })

module.exports = model("ecole", ecoleSchema);