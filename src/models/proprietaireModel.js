const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const proprietaireSchema = new Schema({
    "adresse": String,
    "certificatDelivre": Date,
    "dateNaissance": Date,
    "nom": String,
    "origine": String,
    "prenom": String,
    "chiens": [
      {
        "nom": String,
        "male": Boolean,
        "identificationAmicus": String,
        "dateNaissance": Date,
        "race": String,
      }
    ],
    "localite": String,
    "npa": Number,
    "mails": [
      {
        "mail": String,
        "type": String
      }
    ],
    "telephones": [
      {
        "numero": String,
        "type": String
      }
    ]
  }, { collection: "proprietaire" })

module.exports = model("proprietaire", proprietaireSchema);