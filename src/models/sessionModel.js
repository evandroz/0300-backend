const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const sessionSchema = new Schema({
    "date": Date,
    "dureeM": Number,
    "places": Number,
    "participations": [
      {
        "avecProprietaire": Boolean,
        "chien_id": mongoose.ObjectId
        }
    ],
    "cours_id": mongoose.ObjectId,
    "moniteur_id": mongoose.ObjectId,
    }, { collection: "session" })

module.exports = model("session", sessionSchema);