const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

const sessionModel = require('../models/sessionModel')

router.get('/', async (req, res) => {
    const session = await sessionModel.find()
    res.send(session)
})

router.get('/:id', async (req, res) => {
    const session = await sessionModel.findById(req.params.id);
    res.send(session)
})

router.post('/', async (req, res) => {
    const session = await sessionModel.create(req.body);
    res.send(session)
})

router.put('/:id', async (req, res) => {
    const sessionModifie = await sessionModel.findByIdAndUpdate(req.params.id, req.body, { new: true });
    res.send(sessionModifie);
});

router.delete('/:id', async (req, res) => {
    const sessionSupprime = await sessionModel.findByIdAndDelete(req.params.id);
    res.send(sessionSupprime);
});

router.get('/aggregate/unwind', async (req, res) => {
    try {
      const result = await sessionModel.aggregate([
        { $unwind: "$participations" }
      ]);
      res.status(200).json(result);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  });
  
  router.get('/aggregate/lookup', async (req, res) => {
    try {
      const result = await sessionModel.aggregate([
        { $unwind: "$participations" },
        {
          $lookup: {
            from: 'proprietaire',
            localField: 'participations.chien_id',
            foreignField: 'chiens._id',
            as: 'proprietaireDetails'
          }
        },
        { $unwind: "$proprietaireDetails" }
      ]);
      res.status(200).json(result);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  });


  router.get('/aggregate/project', async (req, res) => {
    try {
      const result = await sessionModel.aggregate([
        {
          $project: {
            date: 1,
            dureeM: 1,
            places: 1,
            'participations.avecProprietaire': 1
          }
        }
      ]);
      res.status(200).json(result);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  });


  router.get('/aggregate/group', async (req, res) => {
    try {
      const result = await sessionModel.aggregate([
        { $unwind: "$participations" },
        {
          $group: {
            _id: "$_id",
            totalParticipations: { $sum: 1 },
            date: { $first: "$date" },
            dureeM: { $first: "$dureeM" },
            places: { $first: "$places" }
          }
        }
      ]);
      res.status(200).json(result);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  });

  router.get('/aggregate/match', async (req, res) => {
    try {
      const result = await sessionModel.aggregate([
        { $match: { places: { $gt: 0 } } }
      ]);
      res.status(200).json(result);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  });
  
module.exports = router