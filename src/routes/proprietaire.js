const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

const proprietaireModel = require('../models/proprietaireModel')

router.get('/', async (req, res) => {
    const proprietaire = await proprietaireModel.find()
    res.send(proprietaire)
})

router.get('/:id', async (req, res) => {
    const proprietaire = await proprietaireModel.findById(req.params.id);
    res.send(proprietaire)
})

router.post('/', async (req, res) => {
    const proprietaire = await proprietaireModel.create(req.body);
    res.send(proprietaire)
})

router.put('/:id', async (req, res) => {
    const proprietaireModifie = await proprietaireModel.findByIdAndUpdate(req.params.id, req.body, { new: true });
    res.send(proprietaireModifie);
});

router.delete('/:id', async (req, res) => {
    const proprietaireSupprime = await proprietaireModel.findByIdAndDelete(req.params.id);
    res.send(proprietaireSupprime);
});

module.exports = router