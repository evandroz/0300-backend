const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

const coursModel = require('../models/coursModel')

router.get('/', async (req, res) => {
    const cours = await coursModel.find()
    res.send(cours)
})

router.get('/:id', async (req, res) => {
    const cours = await coursModel.findById(req.params.id);
    res.send(cours)
})

router.post('/', async (req, res) => {
    const cours = await coursModel.create(req.body);
    res.send(cours)
})

router.put('/:id', async (req, res) => {
    const coursModifie = await coursModel.findByIdAndUpdate(req.params.id, req.body, { new: true });
    res.send(coursModifie);
});

router.delete('/:id', async (req, res) => {
    const coursSupprime = await coursModel.findByIdAndDelete(req.params.id);
    res.send(coursSupprime);
});

module.exports = router