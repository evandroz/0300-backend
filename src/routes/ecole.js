const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

const ecoleModel = require('../models/ecoleModel')

router.get('/', async (req, res) => {
    const ecole = await ecoleModel.find()
    res.send(ecole)
})

router.get('/:id', async (req, res) => {
    const ecole = await ecoleModel.findById(req.params.id);
    res.send(ecole)
})

router.post('/', async (req, res) => {
    const ecole = await ecoleModel.create(req.body);
    res.send(ecole)
})

router.put('/:id', async (req, res) => {
    const ecoleModifie = await ecoleModel.findByIdAndUpdate(req.params.id, req.body, { new: true });
    res.send(ecoleModifie);
});

router.delete('/:id', async (req, res) => {
    const ecoleSupprime = await ecoleModel.findByIdAndDelete(req.params.id);
    res.send(ecoleSupprime);
});

module.exports = router